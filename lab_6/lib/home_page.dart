/* LANDING PAGE */

import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.pink[200],
        title: Text(
          'Sentra Vaksin',
          style: TextStyle(
            color: Colors.white,
            fontSize: 18,
          ),
        ),
      ),
      drawer: Container(
        width: 180,
        child: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget> [
              Container(
                height: 60,
                child: DrawerHeader(
                  child: Text(
                    'Sentra Vaksin',
                    style: TextStyle(color: Colors.pink[900], fontSize: 18),
                  ),
                ),
              ),
              ListTile(
                title: const Text(
                  'Jadwal Vaksinasi',
                    style: TextStyle(fontSize: 14),
                ),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
              ListTile(
                title: const Text(
                  'Registrasi',
                  style: TextStyle(fontSize: 14),
                ),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
              ListTile(
                title: const Text(
                  'Lapor Efek Samping',
                  style: TextStyle(fontSize: 14),
                ),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
            ],
          ),
        ),
      ),
      body: SafeArea(
        child: Container(
          height: screenHeight,
          width: screenWidth,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("images/BGImage.png"),
              colorFilter: new ColorFilter.mode(Colors.white.withOpacity(0.5), BlendMode.dstATop),
              fit: BoxFit.cover,
            ),
          ),
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget> [
                  SizedBox(height: 24),
                  Text(
                    'Ayo Vaksin!',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.pink[900],
                      fontSize: 28,
                    ),
                  ),
                  SizedBox(height: 32),
                  Text(
                    'Welcome to Sentra Vaksin!',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.pink[900],
                      fontSize: 16,
                    ),
                  ),
                  SizedBox(height: 16),
                  ConstrainedBox(
                    constraints: BoxConstraints.tightFor(
                      height: 36,
                      width: screenWidth > 350 ? 280 : screenWidth-32,
                    ),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.pink[300],
                      ),
                      onPressed: () {},
                      child: Text(
                        "Lihat Jadwal & Daftar Vaksinasi",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w700,
                          color: Colors.white,
                        ),
                      )
                    ),
                  ),
                  SizedBox(height: 12),
                  ConstrainedBox(
                    constraints: BoxConstraints.tightFor(
                      height: 36,
                      width: screenWidth > 350 ? 280 : screenWidth-32,
                    ),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.pink[200],
                      ),
                      onPressed: () {},
                      child: Text(
                        "Lapor Efek Samping Vaksinasi",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w700,
                          color: Colors.white,
                        ),
                      )
                    ),
                  ),
                  SizedBox(height: 40),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 20, horizontal: 16),
                    width: screenWidth > 600 ? 550 : screenWidth-32,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(width: 0.5, color: Colors.pinkAccent)
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget> [
                        Text(
                          'Syarat Peserta Vaksinasi',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.pink[900],
                            fontSize: 16,
                          ),
                        ),
                        SizedBox(height: 12),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget> [
                            Text(
                              'Berdasarkan Peraturan Menteri Kesehatan Republik Indonesia Nomor 10 tahun 2021 tentang Pelaksanaan Vaksinasi Dalam Rangka Penanggulangan Pandemi Corona Virus Disease 2019 (COVID-19)',
                              textAlign: TextAlign.justify,
                              style: TextStyle(
                                color: Colors.pink[900],
                                fontSize: 10,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(vertical: screenWidth < 400 ? 28 : 12),
                              child: Column(
                                children: <Widget> [
                                  BulletPoint(content: 'Berusia 12 tahun ke atas', width: screenWidth),
                                  BulletPoint(content: 'Sehat', width: screenWidth),
                                  BulletPoint(content: 'Memiliki KTP/Kartu Keluarga', width: screenWidth),
                                  BulletPoint(content: 'Tidak sedang positif COVID-19', width: screenWidth),
                                ]
                              )
                            ),
                            SizedBox(height: 20),
                            Text(
                              'Catatan:',
                              textAlign: TextAlign.justify,
                              style: TextStyle(
                                color: Colors.pink[900],
                                fontSize: 10,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(vertical: screenWidth < 400 ? 28 : 12),
                              child: Column(
                                children: <Widget> [
                                  BulletPoint(content: 'Wajib memiliki surat rekomendasi dokter bagi penderita komorbid berat, autoimun, atau pasien terapi imunosupresan.', width: screenWidth),
                                  BulletPoint(content: 'Penyintas COVID-19 dapat divaksin minimal 1 bulan (bagi gejala ringan) atau 3 bulan (bagi gejala berat) setelah dinyatakan sembuh.', width: screenWidth),
                                  BulletPoint(content: 'Penggunaan AstraZeneca dan Moderna untuk dosis 1 menyesuaikan dengan stok di Fasilitas Kesehatan.', width: screenWidth),
                                  BulletPoint(content: 'Warga KTP DKI Jakarta dapat mendaftar pada hari H untuk vaksinasi di seluruh sentra vaksinasi.', width: screenWidth),
                                  BulletPoint(content: 'Warga KTP Non-DKI Jakarta dapat mendaftar paling cepat 1 hari sebelum vaksinasi​.', width: screenWidth),
                                ]
                              )
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 24),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class BulletPoint extends StatelessWidget {
  late final content;
  late final width;
  BulletPoint({required this.content, required this.width});

  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget> [
        SizedBox(width: 8),
        Text(
          '•',
          style: TextStyle(
            color: Colors.pink[900],
            fontSize: 16,
          ),
        ),
        SizedBox(width: 4),
        Expanded(
          child: Container(
            margin: EdgeInsets.only(bottom: this.width > 350 ? 12 : 16),
            child: Text(
              this.content,
              style: TextStyle(
                color: Colors.pink[900],
                fontSize: 10,
              ),
            ),
          ),
          ),
      ],
    );
  }
}