from django.db import models

class Note(models.Model):
    to = models.TextField(max_length=100, default="")
    dari = models.TextField(max_length=100, default="")
    title = models.TextField(max_length=100, default="")
    message = models.TextField(max_length=100, default="")
