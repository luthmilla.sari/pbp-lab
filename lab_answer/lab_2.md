1) Apakah perbedaan antara JSON dan XML?

JSON (JavaScript Object Notation) menyajikan informasi terorganisir yang mudah dibaca karena JSON di-design menjadi self-describing. Informasi atau data yang disajikan dalam JSON lebih mudah diakses secara logis. Data-data di JSON disimpan dalam bentuk key and value dimana value dapat berupa primitive data type atau berupa object.

XML (eXtensible Markup Language) menyajikan informasi berupa bahasa markup yang dirancang untuk menyimpan data. Berbeda dengan JSON, XML di-design menjadi self-descriptive, sehingga informasi dari data-data yang disimpan bisa dibaca jelas. Informasi yang ditampilkan dalam XML dibungkus dengan tag-tag yang bisa mengirim, menerima, menyimpan, dan menampilkan informasi tersebut.

2) Apakah perbedaan antara HTML dan XML?

HTML (Hypertext Markup Language) adalah bahasa terstandarisasi yang digunakan untuk menampilkan web page pada browser client. HTML menampilkan informasi berupa struktur/layout web page secara semantik. Semua browser client dapat menerjemahkan elemen-elemen HTML tetapi memiliki caranya masing-masing. Jadi, code HTML yang sama bisa diterjemahkan dengan cara yang berbeda di browser yang berbeda tanpa merubah informasi yang disampaikan.

XML (eXtensible Markup Language) juga merupakan bahasa yang digunakan untuk menampilkan web page pada browser. Akan tetapi, bedanya dengan HTML adalah bahasa XML bersifat dinamis dan digunakan untuk mengangkut data, bukan hanya menampilkannya. Tipe bahasa yang digunakan XML merepresentasikan struktur data yang bisa berubah-ubah.

Referensi:
https://www.monitorteknologi.com/perbedaan-json-dan-xml/
https://blogs.masterweb.com/perbedaan-xml-dan-html/#Apa_Itu_HTML
https://gitlab.com/PBP-2021/pbp-lab/-/tree/master/lab_instruction/lab_2
