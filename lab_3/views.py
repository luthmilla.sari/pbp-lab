from django.http import HttpResponseRedirect
from lab_1.models import Friend
from django.shortcuts import render
from lab_3.forms import FriendForm
from django.contrib.auth.decorators import login_required

# @logiin_required itu supaya usernya harus login dulu

@login_required(login_url="/admin/login/")
def index(request):
    friends = Friend.objects.all()
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url="/admin/login/")
def add_friend(request):
    form = FriendForm(request.POST)
    ## sesuai class yang ada di lab_3.forms

    if (form.is_valid and request.method == "POST"):
        form.save()
        return HttpResponseRedirect('/lab-3')

    ## untuk responnya pake template html
    response = {'form': form}
    return render(request, 'lab3_form.html', response)