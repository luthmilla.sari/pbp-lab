from django.urls import path
from lab_3.views import index, add_friend

urlpatterns = [
    path('', index),
    path('add', add_friend)
]
