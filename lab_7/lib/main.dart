import 'package:flutter/material.dart';
import 'registrasi_vaksin.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Sentra Vaksin',
      theme: ThemeData(
        fontFamily: GoogleFonts.ubuntu().toString(),
      ),
      home: RegistrasiVaksin(
        // harusnya ambil dari widget lain, tapi gapapa gini dulu
        tanggal: DateTime.now(),
        lokasi: "Depok",
        jenisVaksin: "Sinovac",
      ),
    );
  }
}