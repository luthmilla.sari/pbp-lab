/* REGISTRASI PAGE */

import 'package:flutter/material.dart';

class RegistrasiVaksin extends StatefulWidget {
  late DateTime tanggal;
  late String lokasi;
  late String jenisVaksin;

  RegistrasiVaksin({required this.tanggal, required this.lokasi, required this.jenisVaksin});

  @override
  _RegistrasiVaksin createState() => _RegistrasiVaksin(
    tanggal: this.tanggal,
    lokasi: this.lokasi,
    jenisVaksin: this.jenisVaksin,
  );
}

class _RegistrasiVaksin extends State<RegistrasiVaksin> {
  late DateTime tanggal;
  late String lokasi;
  late String jenisVaksin;
  TextEditingController namaController = TextEditingController();
  TextEditingController nikController = TextEditingController();
  TextEditingController noTelpController = TextEditingController();

  _RegistrasiVaksin({required this.tanggal, required this.lokasi, required this.jenisVaksin});

  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.of(context).size.height;
    final screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.pink[200],
        title: Text(
          'Sentra Vaksin',
          style: TextStyle(
            color: Colors.white,
            fontSize: 18,
          ),
        ),
      ),
      drawer: Container(
        width: 180,
        child: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget> [
              Container(
                height: 60,
                child: DrawerHeader(
                  child: Text(
                    'Sentra Vaksin',
                    style: TextStyle(color: Colors.pink[900], fontSize: 18),
                  ),
                ),
              ),
              ListTile(
                title: const Text(
                  'Jadwal Vaksinasi',
                    style: TextStyle(fontSize: 14),
                ),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
              ListTile(
                title: const Text(
                  'Registrasi',
                  style: TextStyle(fontSize: 14),
                ),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
              ListTile(
                title: const Text(
                  'Lapor Efek Samping',
                  style: TextStyle(fontSize: 14),
                ),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
            ],
          ),
        ),
      ),
      body: SafeArea(
        child: Container(
          height: screenHeight,
          width: screenWidth,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("images/BGImage.png"),
              colorFilter: new ColorFilter.mode(Colors.white.withOpacity(0.5), BlendMode.dstATop),
              fit: BoxFit.cover,
            ),
          ),
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget> [
                  SizedBox(height: 24),
                  Text(
                    'Registrasi Vaksinasi',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.pink[900],
                      fontSize: 18,
                    ),
                  ),
                  SizedBox(height: 20),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 20, horizontal: 16),
                    width: screenWidth > 400 ? 300 : screenWidth-32,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(width: 0.5, color: Colors.pinkAccent)
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget> [
                        Text(
                          "Registrasi vaksinasi pada tanggal",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.pink[900],
                            fontSize: 12,
                          ),
                        ),
                        SizedBox(height: 4),
                        Text(
                          this.tanggal.day.toString() + "/" + this.tanggal.month.toString() + "/" + this.tanggal.year.toString()
                          + " di " + this.lokasi + ",",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.pink[900],
                            fontSize: 12,
                          ),
                        ),
                        SizedBox(height: 4),
                        Text(
                          "dengan vaksin " + this.jenisVaksin + ".",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.pink[900],
                            fontSize: 12,
                          ),
                        ),
                        SizedBox(height: 24),
                        Text(
                          "Lengkapi data diri Anda:",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.pink[900],
                            fontSize: 12,
                          ),
                        ),
                        SizedBox(height: 8),
                        Container(
                          height: 36,
                          child: TextField(
                            controller: namaController,
                            decoration: InputDecoration(
                              labelText: "Nama",
                              isDense: true,
                              labelStyle: TextStyle(
                                fontSize: 12,
                              ),
                              fillColor: Colors.blueGrey[50],
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(4)),
                                borderSide: BorderSide(width: 0.5, color: Colors.pinkAccent),
                              )
                            )
                          ),
                        ),
                        SizedBox(height: 12),
                        Container(
                          height: 36,
                          child: TextField(
                            controller: nikController,
                            decoration: InputDecoration(
                              labelText: "NIK",
                              isDense: true,
                              labelStyle: TextStyle(
                                fontSize: 12,
                              ),
                              fillColor: Colors.blueGrey[50],
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(4)),
                                borderSide: BorderSide(width: 0.5, color: Colors.pinkAccent),
                              )
                            )
                          ),
                        ),
                        SizedBox(height: 12),
                        Container(
                          height: 36,
                          child: TextField(
                            controller: noTelpController,
                            decoration: InputDecoration(
                              labelText: "No Telepon",
                              isDense: true,
                              labelStyle: TextStyle(
                                fontSize: 12,
                              ),
                              fillColor: Colors.blueGrey[50],
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(4)),
                                borderSide: BorderSide(width: 0.5, color: Colors.pinkAccent),
                              )
                            )
                          ),
                        ),
                        SizedBox(height: 12),
                        ConstrainedBox(
                          constraints: BoxConstraints.tightFor(
                            height: 36,
                            width: screenWidth > 400 ? 280 : screenWidth-32,
                          ),
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              primary: Colors.pink[200],
                            ),
                            onPressed: () {
                              print("Nama: " + namaController.text);
                              print("NIK: " + nikController.text);
                              print("No Telepon: " + noTelpController.text);
                              setState(() {});
                            },
                            child: Text(
                              "Submit",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.w700,
                                color: Colors.white,
                              ),
                            )
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        )
      )   
    );
  }
}